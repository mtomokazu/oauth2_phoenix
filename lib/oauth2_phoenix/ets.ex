defmodule Oauth2Phoenix.Ets do

  def create_tables(tables) do
    Enum.each(tables, fn(table) -> :ets.new(table, [:named_table, :public]) end)
  end

  def drop_tables(tables) do
    Enum.each(tables, fn(table) -> :ets.delete(table) end)
  end

  def get(table, key) do
    case :ets.lookup(table, key) do
      [] -> {:error, :notfound}
      [{_key, value}] -> {:ok, value}
    end
  end

  def put(table, key, value) do
    :ets.insert(table, {key, value})
    :ok
  end

  def delete(table, key) do
    :ets.delete(table, key)
  end

end
