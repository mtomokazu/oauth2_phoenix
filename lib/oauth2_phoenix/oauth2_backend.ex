defmodule Oauth2Phoenix.Oauth2Backend do
  use Application
  alias Oauth2Phoenix.Ets
  alias Oauth2Phoenix.PasswordCredential
  alias Oauth2Phoenix.ClientCredential

  @access_token_table  :access_tokens
  @refresh_token_table :refresh_tokens
  @user_table          :users
  @client_table        :clients

  @tables [@access_token_table, @refresh_token_table, @user_table, @client_table]

  # Flag for using ETS or using database
  @use_ets false

  def start() do
    if @use_ets do
      Ets.create_tables(@tables)
    end
  end

  def stop() do
    if @use_ets do
      Ets.drop_tables(@tables)
    end
  end

  #===================================================================
  # Utility functions
  #===================================================================

  @doc """
  https://github.com/kivra/oauth2_example/blob/master/src/oauth2_example_backend.erl#L96
  """
  def add_user(username, password) do
    put(@user_table, username, %{username: username, password: password})
  end

  @doc """
  https://github.com/kivra/oauth2_example/blob/master/src/oauth2_example_backend.erl#L108
  """
  def add_client(client_id, client_secret, redirect_uri) do
    client = %{client_id: client_id, client_secret: client_secret, redirect_uri: redirect_uri}
    #client = {client_id, client_secret, redirect_uri}
    put(@client_table, client_id, client)
  end

  def add_client(client_id, client_secret) do
    add_client(client_id, client_secret, nil)
  end

  #===================================================================
  # Backend functions
  #===================================================================

#  def authenticate_username_password(Username, Password, _) do
#    case get(?USER_TABLE, Username) do
#      {ok, #user{password = UserPw}} ->
#      case Password do
#      UserPw ->
#         {ok, {"user", Username}};
#      _ ->
#         {error, :badpass}
#      end;
#        Error = {error, :notfound} ->
#        Error
#      end
#    end

  @doc """
  Authenticates a combination of username and password.
  Returns the resource owner identity if the credentials are valid.
  Backend method for password credentials grant.
  https://github.com/kivra/oauth2/blob/0.6.1/src/oauth2_backend.erl#L34
  https://github.com/kivra/oauth2_example/blob/master/src/oauth2_example_backend.erl#L118
  """
  def authenticate_user(user, appctx) do
    # {:ok, {appctx, user}}

    %{username: username, password: password} = user
    case get(@user_table, username) do
      {:ok, %{password: ^password} = user} -> {:ok, {appctx, user}}
      {:ok, %{password: _wrong_password}}  -> {:error, :badpass}
      _                                    -> {:error, :notfound}
    end

  end

# @doc Verifies that scope() is a valid scope for the resource
#       owner identified by Identity.
#-callback verify_resowner_scope(term(), scope(), appctx()) ->
#                    {ok, {appctx(), scope()}} | {error, notfound | badscope}.
  @doc """
  Verifies that 'scope' is a valid scope for the resource.
  Backend method for password credentials grant.
  https://github.com/kivra/oauth2/blob/0.6.1/src/oauth2_backend.erl#L102
  https://github.com/kivra/oauth2_example/blob/master/src/oauth2_example_backend.erl#L57
  """
  def verify_resowner_scope(user, scope, appctx) do
    {:ok, {appctx, user}}
  end

#  def authenticate_client(cid, csecret, _) do
#    case get(CLIENT_TABLE, client_id) do
#      {:ok, %client{client_secret = csecret}} -> {:ok, {"client", client_id}}
#      {:ok, %client{client_secret = _wrong_secret}} -> {:error, :badsecret}
#      _ -> {error, :notfound}
#    end
#  end
  @doc """
  Authenticates a client's credentials for a given scope.
  Backend method for client credentials grant.
  https://github.com/kivra/oauth2/blob/0.6.1/src/oauth2_backend.erl#L38
  https://github.com/kivra/oauth2_example/blob/master/src/oauth2_example_backend.erl#L131
  """
  def authenticate_client(client, appctx) do
    #IO.puts("#{inspect client}")
    # {:ok, {appctx, client}}

    %{client_id: client_id, client_secret: client_secret} = client
#    %{client_id: client_id} = client
#    data = get(@client_table, client_id)
#    IO.puts("#{inspect data}")
    case get(@client_table, client_id) do
#      {:ok, client_secret} -> {:ok, {appctx, client}}
#      {:ok, _wrong_secret} -> {:error, :badsecret}
      {:ok, %{client_secret: ^client_secret} = client} -> {:ok, {appctx, client}}
      {:ok, %{client_secret: _wrong_secret}}           -> {:error, :badsecret}
      _                                                -> {:error, :notfound}
    end
  end

  @doc """
  Verifies that scope() is a valid scope for the client identified by Identity.
  Backend method for client credentials grant.
  https://github.com/kivra/oauth2_example/blob/master/src/oauth2_example_backend.erl#L202
  """
  def verify_client_scope(client, scope, appctx) do
    # #a{client=Client, resowner=Owner, scope=Scope, ttl=TTL}
    {:ok, {appctx, scope}}
    # {:ok, scope}
  end

  # -callback associate_access_token(token(), grantctx(), appctx()) ->
  #                                        {ok, appctx()} | {error, notfound}.
  @doc """
  Stores a new access token token(), associating it with Context.
  The context is a proplist carrying information about the identity
  with which the token is associated, when it expires, etc.
  Backend method for client credentials grant.
  https://github.com/kivra/oauth2_example/blob/master/src/oauth2_example_backend.erl#L155
  """
  def associate_access_token(token, _grantctx, appctx) do
    # put(?ACCESS_TOKEN_TABLE, token, grantctx)
    {:ok, appctx}
    # appctx
    #:ok
  end

  #===================================================================
  # Internal functions
  #===================================================================

  defp to_binary(atom) when is_atom(atom) do
    list_to_binary(atom_to_list(atom))
  end

  defp get(table, key) do
    if @use_ets do
      Ets.get(table, key)
    else
      case table do
        @user_table -> PasswordCredential.get(key)
        @client_table -> ClientCredential.get(key)
        _ -> "Not implemented"
      end
    end
  end

  defp put(table, key, value) do
    #IO.puts("#{inspect key}")
    #IO.puts("#{inspect value}")
    if @use_ets do
      Ets.put(table, key, value)
    else
      case table do
        @user_table -> PasswordCredential.put(key, value)
        @client_table -> ClientCredential.put(key, value)
        _ -> "Not implemented"
      end
    end
  end

  defp delete(table, key) do
    if @use_ets do
      Ets.delete(table, key)
    else
      case table do
        @user_table -> Repo.all(PasswordCredential)
        @client_table -> Repo.all(ClientCredential)
        _ -> "Not implemented"
      end
    end
  end

end
