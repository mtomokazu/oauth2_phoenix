defmodule Oauth2Phoenix.Repo.Migrations.CreateClientCredential do
  use Ecto.Migration

  def change do
    create table(:client_credentials) do
      add :client_id, :string
      add :client_secret, :string
      add :access_token, :string
      add :expires_in, :string
      add :scope, :string

      timestamps
    end

  end
end
