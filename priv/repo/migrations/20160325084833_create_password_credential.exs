defmodule Oauth2Phoenix.Repo.Migrations.CreatePasswordCredential do
  use Ecto.Migration

  def change do
    create table(:password_credentials) do
      add :username, :string
      add :password, :string
      add :access_token, :string
      add :expires_in, :string
      add :scope, :string

      timestamps
    end

  end
end
