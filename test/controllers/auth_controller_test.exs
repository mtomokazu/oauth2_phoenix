defmodule Oauth2Phoenix.AuthControllerTest do
  use Oauth2Phoenix.ConnCase

  @valid_attrs %{user_id: 42}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "No grant_type" do
    conn = post conn(), "/auth"

    assert conn.status == 400
    assert Regex.match?(~r/'grant_type' is bad or missing./, conn.resp_body)
  end

  test "Illegal grant_type" do
    conn = post conn(), "/auth", [grant_type: "dummy"]

    assert conn.status == 400
    assert Regex.match?(~r/'grant_type' is bad or missing./, conn.resp_body)
  end

  test "'Authorization' header is missing for client credentials" do
    conn = conn()
    |> post("/auth", [grant_type: "client_credentials"])

    assert conn.status == 400
    assert Regex.match?(~r/header is missing./, conn.resp_body)
  end

  test "'Authorization' header has no 'Basic' string." do
    conn = conn()
    |> put_req_header("authorization", "corrupt") # Can't decode by Base64
    |> post("/auth", [grant_type: "client_credentials"])

    assert conn.status == 400
    assert Regex.match?(~r/header is corrupt./, conn.resp_body)
  end

  test "'Authorization' header is corrupt for client credentials" do
    conn = conn()
    |> put_req_header("authorization", "Basic corrupt") # Can't decode by Base64
    |> post("/auth", [grant_type: "client_credentials"])

    assert conn.status == 400
    assert Regex.match?(~r/header is corrupt./, conn.resp_body)
  end

  test "Username in 'Authorization' header is wrong for client credentials" do
    conn = conn()
    |> put_req_header("authorization", "Basic bm9vbmU6Zm9v") # noone:foo
    |> post("/auth", [grant_type: "client_credentials"])

    assert conn.status == 400
    assert Regex.match?(~r/invalid_client/, conn.resp_body)
  end

  test "Password in 'Authorization' header is wrong for client credentials" do
    conn = conn()
    |> put_req_header("authorization", "Basic YXF1YTpiYXI=") # aqua:bar
    |> post("/auth", [grant_type: "client_credentials"])

    assert conn.status == 400
    assert Regex.match?(~r/invalid_client/, conn.resp_body)
  end

  test "normal case for client credentials" do
    conn = conn()
    |> put_req_header("authorization", "Basic YXF1YTpmb28=") # aqua:foo
    |> post("/auth", [grant_type: "client_credentials"])

    assert conn.status == 200
    assert json_response(conn, 200)["token_type"] == "bearer"
    assert Regex.match?(~r/\A\w{20,}\z/, json_response(conn, 200)["access_token"])
    #assert json_response(conn, 200)["access_token"]
  end

  test "'username' is missing for password credentials" do
    conn = conn()
    |> post("/auth", [grant_type: "password", password: "any", scope: "any"])

    assert conn.status == 400
    assert Regex.match?(~r/access_denied/, conn.resp_body)
  end

  test "'password' is missing for password credentials" do
    conn = conn()
    |> post("/auth", [grant_type: "password", username: "any", scope: "any"])

    assert conn.status == 400
    assert Regex.match?(~r/access_denied/, conn.resp_body)
  end

  test "'username' is wrong for password credentials" do
    conn = conn()
    |> post("/auth", [grant_type: "password", username: "any", password: "wrong_password", scope: "any"])

    assert conn.status == 400
    assert Regex.match?(~r/access_denied/, conn.resp_body)
  end

  test "'password' is wrong for password credentials" do
    conn = conn()
    |> post("/auth", [grant_type: "password", username: "any", password: "wrong_password", scope: "any"])

    assert conn.status == 400
    assert Regex.match?(~r/access_denied/, conn.resp_body)
  end

  test "normal case for password credentials" do
    conn = conn()
    |> post("/auth", [grant_type: "password", username: "bob", password: "luv_alice", scope: "any"])

    #IO.puts("Normal = #{conn.resp_body}")
    assert conn.status == 200
    assert json_response(conn, 200)["token_type"] == "bearer"
    assert Regex.match?(~r/\A\w{20,}\z/, json_response(conn, 200)["access_token"])
  end

#  test "shows chosen resource", %{conn: conn} do
#    auth = Repo.insert! %Auth{}
#    conn = get conn, auth_path(conn, :show, auth)
#    assert json_response(conn, 200)["data"] == %{"id" => auth.id,
#      "user_id" => auth.user_id}
#  end
#
#  test "does not show resource and instead throw error when id is nonexistent", %{conn: conn} do
#    assert_error_sent 404, fn ->
#      get conn, auth_path(conn, :show, -1)
#    end
#  end
#
#  test "creates and renders resource when data is valid", %{conn: conn} do
#    conn = post conn, auth_path(conn, :create), auth: @valid_attrs
#    assert json_response(conn, 201)["data"]["id"]
#    assert Repo.get_by(Auth, @valid_attrs)
#  end
#
#  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
#    conn = post conn, auth_path(conn, :create), auth: @invalid_attrs
#    assert json_response(conn, 422)["errors"] != %{}
#  end
#
#  test "updates and renders chosen resource when data is valid", %{conn: conn} do
#    auth = Repo.insert! %Auth{}
#    conn = put conn, auth_path(conn, :update, auth), auth: @valid_attrs
#    assert json_response(conn, 200)["data"]["id"]
#    assert Repo.get_by(Auth, @valid_attrs)
#  end
#
#  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
#    auth = Repo.insert! %Auth{}
#    conn = put conn, auth_path(conn, :update, auth), auth: @invalid_attrs
#    assert json_response(conn, 422)["errors"] != %{}
#  end
#
#  test "deletes chosen resource", %{conn: conn} do
#    auth = Repo.insert! %Auth{}
#    conn = delete conn, auth_path(conn, :delete, auth)
#    assert response(conn, 204)
#    refute Repo.get(Auth, auth.id)
#  end

end
