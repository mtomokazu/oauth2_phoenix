defmodule Oauth2Phoenix.ClientCredentialTest do
  use Oauth2Phoenix.ModelCase

  alias Oauth2Phoenix.ClientCredential

  @valid_attrs %{access_token: "some content",
                 client_id: "some content",
                 client_secret: "some content",
                 expires_in: "some content",
                 scope: "some content"}
  @invalid_attrs %{}

  test "put" do
    value = %{client_id: "foo", client_secret: "bar", redirect_uri: nil}
    put = ClientCredential.put("foo", value)
    assert :ok == put
  end

  test "get" do
    value = %{client_id: "foo", client_secret: "bar", redirect_uri: nil}
    put = ClientCredential.put("foo", value)
    assert :ok == put

    got = ClientCredential.get("foo")
    assert {:ok, _} = got
  end

  test "delete" do
    value = %{client_id: "foo", client_secret: "bar", redirect_uri: nil}
    put = ClientCredential.put("foo", value)
    assert :ok == put

    deleted = ClientCredential.delete("foo")
    assert {:ok, _} = deleted

    got = ClientCredential.get("foo")
    assert is_nil(got)
  end

  test "changeset with valid attributes" do
    changeset = ClientCredential.changeset(%ClientCredential{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = ClientCredential.changeset(%ClientCredential{}, @invalid_attrs)
    refute changeset.valid?
  end
end
