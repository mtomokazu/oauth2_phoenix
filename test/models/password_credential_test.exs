defmodule Oauth2Phoenix.PasswordCredentialTest do
  use Oauth2Phoenix.ModelCase

  alias Oauth2Phoenix.PasswordCredential

  @valid_attrs %{access_token: "some content",
                 expires_in: "some content",
                 password: "some content",
                 scope: "some content",
                 username: "some content"}
  @invalid_attrs %{}

  test "put" do
    value = %{username: "foo", password: "bar"}
    put = PasswordCredential.put("foo", value)
    assert :ok == put
  end

  test "get" do
    value = %{username: "foo", password: "bar"}
    put = PasswordCredential.put("foo", value)
    assert :ok == put

    got = PasswordCredential.get("foo")
    assert {:ok, _} = got
  end

  test "delete" do
    value = %{username: "foo", password: "bar"}
    put = PasswordCredential.put("foo", value)
    assert :ok == put

    deleted = PasswordCredential.delete("foo")
    assert {:ok, _} = deleted

    got = PasswordCredential.get("foo")
    assert is_nil(got)
  end

  test "changeset with valid attributes" do
    changeset = PasswordCredential.changeset(%PasswordCredential{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = PasswordCredential.changeset(%PasswordCredential{}, @invalid_attrs)
    refute changeset.valid?
  end

end
