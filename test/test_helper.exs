ExUnit.start

Mix.Task.run "ecto.create", ~w(-r Oauth2Phoenix.Repo --quiet)
Mix.Task.run "ecto.migrate", ~w(-r Oauth2Phoenix.Repo --quiet)
Ecto.Adapters.SQL.begin_test_transaction(Oauth2Phoenix.Repo)

