defmodule Oauth2Phoenix.AuthController do
  use Oauth2Phoenix.Web, :controller
  alias Oauth2Phoenix.Repo
  alias Oauth2Phoenix.Auth

#  use Oauth2Phoenix.Oauth2Backend

  plug :scrub_params, "auth" when action in [:create, :update]

#  def index(conn, _params) do
#    auth = Repo.all(Auth)
#    render(conn, "index.json", auth: auth)
#  end

  def index(conn, params) do
    #:application.start(:oauth2)
    :application.set_env(:oauth2, :expiry_time, 3600)
    :application.set_env(:oauth2, :backend, Oauth2Phoenix.Oauth2Backend)
    Oauth2Phoenix.Oauth2Backend.start()
    Oauth2Phoenix.Oauth2Backend.add_client("aqua", "foo")
    Oauth2Phoenix.Oauth2Backend.add_user("bob", "luv_alice")

    case Map.get(params, "grant_type") do
      "password"           -> process_password_grant(conn, params)
      "client_credentials" -> process_client_credentials_grant(conn, params);
      #"token"              -> process_implicit_grant_stage2(conn, params);
      _                    -> send_error(conn, 400, "'grant_type' is bad or missing.")
    end
  end

  defp send_error(conn, status_code, message) do
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(status_code, :jsx.encode([ {"message", message} ]))
  end

  defp process_password_grant(conn, params) do
    #username = :proplists.get_value("username", params)
    username = Map.get(params, "username")
    password = Map.get(params, "password")
    scope    = Map.get(params, "scope", "")

    user = %{username: username, password: password}
    auth = :oauth2.authorize_password(user, scope, [])
    issue_token(auth, conn)
  end

  defp process_client_credentials_grant(conn, params) do
    credential = List.keyfind(conn.req_headers, "authorization", 0)
    if is_nil(credential) do
      send_error(conn, 400, "'Authorization' header is missing.")
    else
      credential = elem(credential, 1)
      #IO.puts("#{inspect credential}")
      try do
        matcher = Regex.run(~r/\ABasic (\S+)\z/, credential)
        [id, secret] =
          case matcher do
            [_, base64str] -> base64str |> Base.decode64! |> String.split(":")
            _                       -> raise ArgumentError
          end
        scope = params["scope"]

        # -spec authorize_client_credentials(client(), scope(), appctx())
        #                             -> {ok, {appctx(), auth()}} | {error, error()}.
        # authorize_client_credentials(Client, Scope0, Ctx0) ->
        client = %{client_id: id, client_secret: secret}
        auth = :oauth2.authorize_client_credentials(client, scope, :context)
        issue_token(auth, conn)
      rescue
        e in ArgumentError -> send_error(conn, 400, "'Authorization' header is corrupt. #{e.message}")
      end
    end
  end

  #defp issue_token(auth, conn) do
  defp issue_token({:ok, auth}, conn) do
    # -spec issue_token(auth(), appctx()) -> {ok, {appctx(), response()}}.
    # issue_token(#a{client=Client, resowner=Owner, scope=Scope, ttl=TTL}, Ctx0) ->

    #IO.puts("#{inspect auth}")
    {_context, a} = auth
    auth_result = :oauth2.issue_token(a, [])
    #auth_result = { :auth, 200, "OK" }
    emit_response(auth_result, conn)
  end

  defp issue_token({:error, reason}, conn) do
    emit_response({:error, reason}, conn)
  end

  defp emit_response(auth_result, conn) do
    #IO.puts("auth_result")
    #IO.puts("#{inspect auth_result}")
    {status_code, auth_data} =
      case auth_result do
        {:error, reason} ->
          {400, :jsx.encode([ {:error, to_binary(reason)} ])}
        response ->
          # -spec to_proplist(response()) -> proplists:proplist().
          # to_proplist(Response) ->
          {_code, {_data, resp}} = response
          {200, :jsx.encode(to_json_term(:oauth2_response.to_proplist(resp), []))}
          #{200, to_json_term(:oauth2_response.to_proplist(resp), [])}
          # {200, :oauth2_response.to_proplist(resp)}
          # {200, to_json_term(resp, [])}
          # {200, resp}
      end
    #IO.puts("#{inspect auth_data}")
    #render(conn, "index.json", auth: {code, auth_data})
    #render(conn, "index.json", auth: auth_data)
    #json(conn, auth_data)

    conn |> put_resp_content_type("application/json") |> send_resp(status_code, auth_data)
  end

  defp to_binary(atom) when is_atom(atom) do
    :erlang.list_to_binary(:erlang.atom_to_list(atom))
  end

  defp to_json_term([], acc) do
    acc
  end

  defp to_json_term([{hd, {key, val}} |  tl], acc) do
    to_json_term(tl, [{hd, "{", key, ",", val, "}"} | acc])
  end

  defp to_json_term([hd | tl], acc) do
    to_json_term(tl, [hd | acc])
  end

  # -----------------------------------------------------------------

  def create(conn, %{"auth" => auth_params}) do
    changeset = Auth.changeset(%Auth{}, auth_params)

    case Repo.insert(changeset) do
      {:ok, auth} ->
        conn
        |> put_status(:created)
        |> put_resp_header("location", auth_path(conn, :show, auth))
        |> render("show.json", auth: auth)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Oauth2Phoenix.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    auth = Repo.get!(Auth, id)
    render(conn, "show.json", auth: auth)
  end

  def update(conn, %{"id" => id, "auth" => auth_params}) do
    auth = Repo.get!(Auth, id)
    changeset = Auth.changeset(auth, auth_params)

    case Repo.update(changeset) do
      {:ok, auth} ->
        render(conn, "show.json", auth: auth)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Oauth2Phoenix.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    auth = Repo.get!(Auth, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(auth)

    send_resp(conn, :no_content, "")
  end

end
