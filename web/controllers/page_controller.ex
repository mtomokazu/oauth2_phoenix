defmodule Oauth2Phoenix.PageController do
  use Oauth2Phoenix.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
