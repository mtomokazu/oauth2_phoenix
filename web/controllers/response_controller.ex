defmodule Oauth2Phoenix.ResponseController do
  use Oauth2Phoenix.Web, :controller

  # alias Oauth2Phoenix.Response

  plug :scrub_params, "response" when action in [:create, :update]

  def index(conn, _params) do
    # response = Repo.all(Response)
    render(conn, "index.json")
  end

end
