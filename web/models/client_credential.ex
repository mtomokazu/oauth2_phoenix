defmodule Oauth2Phoenix.ClientCredential do
  use Oauth2Phoenix.Web, :model

  alias Oauth2Phoenix.ClientCredential
  alias Oauth2Phoenix.Repo
  import Ecto.Query

  schema "client_credentials" do
    field :client_id, :string
    field :client_secret, :string
    field :access_token, :string
    field :expires_in, :string
    field :scope, :string

    timestamps
  end

  @required_fields ~w(client_id client_secret access_token expires_in scope)
  @optional_fields ~w()

  def get(key) when is_nil(key) do
    nil
  end

  def get(key) do
    query = from c in ClientCredential,
    where: c.client_id == ^key,
    select: c
    credential = Repo.one(query)
    if credential == nil do
      nil
    else
      # Construct like ETS
      {:ok, %{client_id: credential.client_id, client_secret: credential.client_secret}}
    end
  end

  def put(key, value) do
    credential = %ClientCredential{client_id: key, client_secret: value[:client_secret]}
    Repo.insert!(credential)
    #cred = Repo.all(ClientCredential)
    #IO.puts("#{inspect cred}")
    :ok
  end

  def delete(key) do
    credential = Repo.get_by(ClientCredential, client_id: key)
    Repo.delete(credential)
  end

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end
