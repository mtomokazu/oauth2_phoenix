defmodule Oauth2Phoenix.PasswordCredential do
  use Oauth2Phoenix.Web, :model

  alias Oauth2Phoenix.PasswordCredential
  alias Oauth2Phoenix.Repo
  import Ecto.Query

  schema "password_credentials" do
    field :username, :string
    field :password, :string
    field :access_token, :string
    field :expires_in, :string
    field :scope, :string

    timestamps
  end

  @required_fields ~w(username password access_token expires_in scope)
  @optional_fields ~w()

  def get(key) when is_nil(key) do
    nil
  end

  def get(key) do
    query = from p in PasswordCredential,
    where: p.username == ^key,
    select: p
    credential = Repo.one(query)
    if credential == nil do
      nil
    else
      # Construct like ETS
      {:ok, %{username: credential.username, password: credential.password}}
    end
  end

  def put(key, value) do
    credential = %PasswordCredential{username: key, password: value[:password]}
    Repo.insert!(credential)
    #cred = Repo.all(PasswordCredential)
    #IO.puts("#{inspect cred}")
    :ok
  end

  def delete(key) do
    credential = Repo.get_by(PasswordCredential, username: key)
    Repo.delete(credential)
  end

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end
