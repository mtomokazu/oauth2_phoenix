defmodule Oauth2Phoenix.Router do
  use Oauth2Phoenix.Web, :router

  pipeline :browser do
    plug :accepts, ["html", "json"]
    plug :fetch_session
    plug :fetch_flash
#    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Oauth2Phoenix do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  scope "/auth", Oauth2Phoenix do
    pipe_through :browser

    post "/",         AuthController, :index
#    post "/",         AuthController, :index/2
#    get "/:provider", AuthController, :index
    get "/:provider/callback", AuthController, :callback
    delete "/logout", AuthController, :delete
  end

  scope "/response", Oauth2Phoenix do
    pipe_through :browser

    post "/",         ResponseController, :index
    get "/:provider", ResponseController, :index
    get "/:provider/callback", ResponseController, :callback
    delete "/logout", ResponseController, :delete
  end

end
